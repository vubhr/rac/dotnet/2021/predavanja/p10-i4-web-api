namespace ConfApp.Models;

public class ConferenceService : IConferenceService {
  public ConferenceService() {
    conferences = new List<Conference>();
    conferences.AddRange(
      new Conference[] {
        new Conference("B:it.con", "Bitcon", new List<string>()),
        new Conference("Kulen dayz", "Kulenz", new List<string>())
      }
    );
  }

  public IEnumerable<Conference> GetConferences() {
    return conferences;
  }

  public void AddConference(Conference conference) {
    conferences.Add(conference);
  }

  public Conference GetConference(string permalink) {
    var conference = from c in conferences
      where c.Permalink == permalink
      select c;
    if (conference.ToList().Count == 1) {
      return conference.First();
    } else {
      return null;
    }
  }

  private List<Conference> conferences;
}