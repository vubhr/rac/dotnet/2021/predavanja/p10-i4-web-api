namespace ConfApp.Models;

public interface IConferenceService {
  IEnumerable<Conference> GetConferences();
  Conference GetConference(string permalink);
  void AddConference(Conference conference);
}