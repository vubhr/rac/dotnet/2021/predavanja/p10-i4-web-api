namespace ConfApp.Models;

public class Conference {
  public Conference(string? title, string? permalink, List<string> speakers) {
    Title = title;
    Permalink = permalink;
    Speakers = speakers;
  }
  public string? Title { get; set; }
  public string? Permalink { get; set; }
  public List<string> Speakers { get; set; }
}
