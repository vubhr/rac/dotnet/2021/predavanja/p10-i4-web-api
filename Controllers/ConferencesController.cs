using Microsoft.AspNetCore.Mvc;
using System.Collections;
using ConfApp.Models;

namespace ConfApp.Controllers;

[Route("api/[controller]")]
public class ConferencesController : ControllerBase {

  private readonly IConferenceService conferenceService;

  public ConferencesController(IConferenceService conferenceService) {
    this.conferenceService = conferenceService;
  }

  [HttpGet]
  public IActionResult GetConferences() {
    try {
      IEnumerable results = conferenceService.GetConferences();
      return Ok(results);
    } catch (Exception) {
      return StatusCode(StatusCodes.Status500InternalServerError, "Error while retrieving data");
    }
  }

  [HttpGet("{permalink}")]
  public ActionResult<Conference> Get(string permalink) {
    var conference = conferenceService.GetConference(permalink);
    if (conference != null) {
      return Ok(conference);
    } else {
      return NotFound("Can't find it");
    }
  }

  [HttpPost]
  public IActionResult Post(string title, string permalink) {
    var conference = new Conference(title, permalink, new List<string>());
    conferenceService.AddConference(conference);
    return Created("/1", conference);
  }
}
